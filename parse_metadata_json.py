import json
import sys
import pprint
file_str = sys.argv[1:][0]

#print("path:",file_str)
fp = open(file_str, "r")
loaded_json = json.load(fp)
obj721 = next(x for x in loaded_json.keys())
txhash = next(x for x in loaded_json[obj721])
names_json = loaded_json[obj721][txhash]
quantities = []
for k in names_json.keys():
    print(k,end=" ")
    try:
        q = names_json[k]['quantity']
        quantities.append(q)
    except Exception as e:
        #print(e.message, e.args)
        print("\nquantity not found in json name:",k)
        sys.exit()
print("")
for q in quantities:
    print(q,end = " ")