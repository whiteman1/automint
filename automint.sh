export CARDANO_NODE_SOCKET_PATH=~/Library/Application\ Support/Daedalus\ Mainnet/cardano-node.socket

	function gen_address_policy () {
		./cardano-node-1.26.1-macos/cardano-cli address key-gen \
		--verification-key-file ./wallet_$1/payment.vkey \
		--signing-key-file ./wallet_$1/payment.skey

		./cardano-node-1.26.1-macos/cardano-cli address build \
		--payment-verification-key-file ./wallet_$1/payment.vkey \
		--out-file ./wallet_$1/payment.addr \
		--mainnet
		
		./cardano-node-1.26.1-macos/cardano-cli  query protocol-parameters \
		--mainnet \
		--out-file protocol.json

		./cardano-node-1.26.1-macos/cardano-cli address key-gen \
		--verification-key-file ./wallet_$1/policy.vkey \
		--signing-key-file ./wallet_$1/policy.skey
	}

	function query_utxo () {
		TX_HASH_LINE=$(./cardano-node-1.26.1-macos/cardano-cli query utxo --address $(cat ./wallet_$1/payment.addr) \
		--mainnet | tail -1)
	#	echo "Quary UTXO: $TX_HASH_LINE"
		TX_HASH=$(echo $TX_HASH_LINE | cut -d' ' -f1)
		if [ $TX_HASH == "--------------------------------------------------------------------------------------" ]; then
			echo "There is no TX_IN in the created wallet yet"
			echo "Need to send 5 ADA to address:"
			cat ./wallet_$1/payment.addr
			echo ""
			echo "Then re-run the script"
		else
			echo $TX_HASH
		fi
	}


	function mint () {
			TX_HASH_LINE=$(./cardano-node-1.26.1-macos/cardano-cli query utxo --address $(cat ./wallet_$1/payment.addr) \
		--mainnet | tail -1)

		echo "Query UTXO: $TX_HASH_LINE"

		TX_HASH=$(echo $TX_HASH_LINE | cut -d' ' -f1)
		echo "TX HASH is:"
		echo $TX_HASH

		echo "Generating Key Hash:"

		KEY_HASH=$(./cardano-node-1.26.1-macos/cardano-cli address key-hash --payment-verification-key-file ./wallet_$1/policy.vkey)

		echo $KEY_HASH
		echo ""
		echo "Generating Policy script within ./wallet_$1/"
		echo ""
		echo "{ \"keyHash\": \"$KEY_HASH\", \"type\": \"sig\"}" > ./wallet_$1/policy_$1.script

		cat "./wallet_$1/policy_$1.script"

		ASSET_NAME=$(./cardano-node-1.26.1-macos/cardano-cli transaction policyid --script-file ./wallet_$1/policy_$1.script)
		
		echo "{" > ./wallet_$1/$1_metadata_template.json 
	    echo "	\"721\":{" >> ./wallet_$1/$1_metadata_template.json
	   	echo "		\"$ASSET_NAME\":{" >> ./wallet_$1/$1_metadata_template.json
		echo "			\"NFTnameHere1\":{" >> ./wallet_$1/$1_metadata_template.json
		echo "				\"id\":1," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"image\":\"ipfs://IPFS_CID_HERE\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"name\":\"NFT_name_here_1\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"quantity\":\"1\"," >> ./wallet_$1/$1_metadata_template.json		
		echo "				\"copyright\":\"© 2021 #E0\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"site\":\"https://ne0.xyz\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"twitter\":\"https://twitter.com/whitemane0\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"etc\":\"somethingelse\"" >> ./wallet_$1/$1_metadata_template.json
		echo "			}," >> ./wallet_$1/$1_metadata_template.json
		echo "			\"NFTnameHere2\":{" >> ./wallet_$1/$1_metadata_template.json
		echo "				\"id\":2," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"image\":\"ipfs://IPFS_CID_HERE\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"name\":\"NFT_name_here_2\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"quantity\":\"1\"," >> ./wallet_$1/$1_metadata_template.json		
		echo "				\"copyright\":\"© 2021 #E0\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"site\":\"https://ne0.xyz\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"twitter\":\"https://twitter.com/whitemane0\"," >> ./wallet_$1/$1_metadata_template.json
		echo "				\"etc\":\"somethingelse\"" >> ./wallet_$1/$1_metadata_template.json
		echo "			}" >> ./wallet_$1/$1_metadata_template.json
		echo "		}" >> ./wallet_$1/$1_metadata_template.json
		echo "	}" >> ./wallet_$1/$1_metadata_template.json
		echo "}" >> ./wallet_$1/$1_metadata_template.json
		echo ""
		echo "Metadata JSON Template was genereted: ./wallet_$1/$1_metadata_template.json"
		echo "Edit ./wallet_$1/$1_metadata_template.json and save it as ./wallet_$1/$1_metadata.json:"  

		cat ./wallet_$1/$1_metadata_template.json

		echo ""
		echo "Provide a path to your edited Metadata JSON File or press Enter to load $meta_json_path"
		read meta_json_path
		if [ $meta_json_path=="" ]; then
			meta_json_path=./wallet_$1/$1_metadata.json
			echo "loading from default path for edited file: $meta_json_path"
		fi

		echo "Metadata JSON path: $meta_json_path"

		JSON_PARSED_NAME_LINE=$(python3 parse_metadata_json.py $meta_json_path | head -1)
		JSON_PARSED_QUANTITY_LINE=$(python3 parse_metadata_json.py $meta_json_path | tail -1)
		JSON_PARSED_NAME=(`echo ${JSON_PARSED_NAME_LINE}`);
		JSON_PARSED_QUANTITY=(`echo ${JSON_PARSED_QUANTITY_LINE}`);

		echo $JSON_PARSED_NAME_LINE
		echo $JSON_PARSED_QUANTITY_LINE

		i=0

		echo ""
		echo "Going to mint:" 
		echo ""



		for n in $JSON_PARSED_NAME_LINE
		do
			echo "${JSON_PARSED_QUANTITY[i]} Asset(s) with name $n"
			
			mint_line="$mint_line${JSON_PARSED_QUANTITY[i]} $ASSET_NAME.$n+"

			i=$(expr $i + 1)
		done

		mint_line=${mint_line%?}

		read -n 1 -s -r -p "If the above correct, hit any button to mint"
		echo ""
		echo "$mint_line"
		echo "$2"
		echo "$TX_HASH#0"


		./cardano-node-1.26.1-macos/cardano-cli transaction build-raw \
		--mary-era \
		--fee 900000 \
		--tx-in $TX_HASH#0 \
		--tx-out $2+14100000+"$mint_line" \
		--mint="$mint_line" \
		--metadata-json-file $meta_json_path \
		--json-metadata-no-schema \
		--out-file ./wallet_$1/matx.raw

		./cardano-node-1.26.1-macos/cardano-cli transaction sign \
		--signing-key-file ./wallet_$1/payment.skey \
		--signing-key-file ./wallet_$1/policy.skey \
		--script-file ./wallet_$1/policy_$1.script \
		--mainnet \
		--tx-body-file ./wallet_$1/matx.raw \
		--out-file ./wallet_$1/matx.signed

		./cardano-node-1.26.1-macos/cardano-cli transaction submit --tx-file  ./wallet_$1/matx.signed --mainnet
	}

	if [ $# -eq 0 ]; then
		echo ""
		echo "Usage: bash automint.sh <project name> <Address to send tokens>"
		echo ""
		exit 1
	else

	if [ ! -f "./wallet_$1/payment.addr" ]
	then
	    echo "Wallet ./wallet_$1 was not previosly used. Crating Wallet ./wallet_$1"
	    mkdir ./wallet_$1
		gen_address_policy "$1"
		echo "Send 5ADA to the following address:"
		cat ./wallet_$1/payment.addr
		read -n 1 -r -s -p $'\nPress any key when transaction successful\n'
		query_utxo "$1"
		mint "$1" "$2"

	else
		echo "Wallet ./wallet_$1 exists. You can not overwrite existing wallet"
		echo "Proceeding to Query UTXO"
		query_utxo "$1"
		mint "$1" "$2"

	fi
fi	