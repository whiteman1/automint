# README #

This README describes how to use *"automint"* scripts for Cardano Native Tokens minting.

### Cardano CLI binaries ###

./cardano-node-1.26.1-macos/ - Mac OS binaries of Cardano CLI builded by IOHK Hydra 

### Mint single Token ###

For minting single token *./automint_single.sh* provided.
It require only Bash command enterpreter. Every Mac have bash by default.

Go to the directory where you clone this repository
`cd /path_to/automint/`
Run
`bash ./automint_single.sh`

You'll see help for command line arguments:
`Usage: bash automint_single.sh <Token Name> <Quantity> <Address to send tokens>` 

Example:
In this example we going to mint 1 *"AxiSnake"* token, so it will be NFT. 
You can mint several by setting number at the end of line, but Wisemans says it will be not NFT.

`bash ./automint_single.sh AxiSnake 1 addr1qxyftnq680a6ag0gu96znftcyek5gh8kzjmuvm00jlgnk39fsare778tzlhkycm7hdqm54r4rncjqs5t2h78mklqv0js5ktakk`

You'll see promt:

`Send 5ADA to the following address:
addr1v93jv6vhmlv6fr6gxfu60tkyw8k7xxx5kuq9jkh42mgmw2qhrkvmf
Press any key when transaction successful`

Wait until trunsaction is done and hit any button.

You'll see a message:
`Metadata JSON file was genereated as ./wallet_AxiSnake/AxiSnake_metadata_template.json
Edit it to plase your IPFS link, twitter etc.
Save file as ./wallet_AxiSnake/AxiSnake_metadata.json`

Provide a path to edited Metadata JSON or just hit "Enter" if saved as ./wallet_AxiSnake/AxiSnake_metadata.json

You'll see victorious message:
`Transaction successfully submitted.`


### Mint single Token ###

For minting single token *./automint.sh* provided.
It require Bash and Python3 for Metadata JSON parsing.

Go to the directory where you clone this repository
`cd /path_to/automint/`
Run
`bash ./automint_single.sh`

You'll see help for command line arguments:
`Usage: Usage: bash automint.sh <project name> <Address to send tokens>`

Example:

`bash ./automint_single.sh MyProject addr1qxyftnq680a6ag0gu96znftcyek5gh8kzjmuvm00jlgnk39fsare778tzlhkycm7hdqm54r4rncjqs5t2h78mklqv0js5ktakk`

You'll see promt:

`Send 5ADA to the following address:
addr1v93jv6vhmlv6fr6gxfu60tkyw8k7xxx5kuq9jkh42mgmw2qhrkvmf
Press any key when transaction successful`

Wait until trunsaction is done and hit any button.

You'll see a message:
`Metadata JSON Template was genereted: ./wallet_MyProject/MyProject_metadata_template.json
Edit ./wallet_MyProject/MyProject_metadata_template.json and save it as ./wallet_MyProject/MyProject_metadata.json`

Edit ./wallet_MyProject/MyProject_metadata_template.json

Token Names for minting will be taken from JSON. Don't forget to change them.
You can set "quantity" more then 1.
Everything else is as usual IPFS_CID, twitter etc.

Provide a path to edited Metadata JSON or just hit "Enter" if saved as ./wallet_AxiSnake/AxiSnake_metadata.json

You'll see victorious message" 

`Transaction successfully submitted.`


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

Please visit my Cardano Development Home: [https://ne0.xyz](https://ne0.xyz) ©2021 ![#E0](https://ne0.xyz/e0.gif)