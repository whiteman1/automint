import os
import json
import subprocess
import time
CARDANO_CLI = "E:\\Daedalus Mainnet\\cardano-cli.exe"
PIPE_NAME=subprocess.check_output(["powershell","(Get-ChildItem \\\.\\\pipe\\\ | Where-Object {$_.name -like 'cardano-node-mainnet*'}).FullName"])
PIPE_NAME = str(PIPE_NAME).split('.')
PIPE_NAME = "\\\.\\pipe\\cardano-node-mainnet."+PIPE_NAME[2]+"."+PIPE_NAME[3][0]
print("PIPE_NAME:",PIPE_NAME)
os.environ['CARDANO_NODE_SOCKET_PATH'] = PIPE_NAME


'''from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
def getExplorerLastBlock():
    html = urlopen("https://explorer.cardano.org/en.html").read()
    print(html)
    soup = BeautifulSoup(html,features="html.parser")
    for divparent in soup.find_all("div", class_="blocksSlots"):
        print(divparent)
'''
def getTipBlockNum():
    out = subprocess.check_output([CARDANO_CLI,"get-tip" ,"--mainnet"])
    #print(out)
    json_loaded = json.loads(out)
    return json_loaded['block']

#cardano-cli query tip --mainnet

LAST_BLOCK = 6159981
#LAST_BLOCK = getExplorerLastBlock()
while 1:
    print(100.*getTipBlockNum()/LAST_BLOCK)
    time.sleep(1)