# cardano-node-1.26.1-macos
 
*This is a quick guide on how to run a cardano node locally on a Mac OS for minting.*
 
1. Open a terminal and go to your home directory:
```bash
cd ~
``` 
2. First clone cardano node files from this repo into a "cardano-node" directory inside your home directory (~%)
3. Create a "workdir" in you home directory. This is where we will be running all the commands and should be on the same level as the "cardano-node" directory.
```bash
mkdir workdir
``` 
4. CD into "workdir". Create another directory called "configuration". Then CD into "configuration" and create another directory called "cardano"
```bash
cd workdir
mkdir -p configuration/cardano
``` 
6. Inside your config/cardano directory, you will download the config files for mainnet. https://docs.cardano.org/projects/cardano-node/en/latest/stake-pool-operations/getConfigFiles_AND_Connect.html 
7. CD back to your "workdir" directory and run: 
```bash
../cardano-node/cardano-node run --socket-path node.socket
``` 
This is start syncing your node. Generally it takes around a day to a day and a half to sync node completely.
9. Once node is synced, open a **new terminal** at "workdir". Set environment variable for cardano-cli pointing to socket created by the cardano-node:
```bash
export CARDANO_NODE_SOCKET_PATH=node.socket
``` 

## Minting with cardano-cli:

1. Generate Payment Keys:
```bash
../cardano-node/cardano-cli address key-gen \
--verification-key-file payment.vkey \
--signing-key-file payment.skey
```
2. Generate Stake Keys:
```bash
../cardano-node/cardano-cli stake-address key-gen \
--verification-key-file stake.vkey \
--signing-key-file stake.skey
```
3. Generate Payment Address:
```bash
../cardano-node/cardano-cli address build \
--payment-verification-key-file payment.vkey \
--stake-verification-key-file stake.vkey \
--out-file payment.addr \
--mainnet
```
4. Show address to send ADA:
```bash
cat ./wallet/payment.addr
```
Send 5 ADA from Daedalus or Yoroi to the address above. (It's enough for most cases)

5. Check for ada and TX hash:
```bash
../cardano-node/cardano-cli query utxo --address $(cat ./wallet/payment.addr) --mainnet
```
If you just send you may need to wait about a minute. If query utxo reports nothing, wait 30 seconds and try again.

6. Create protocol.json file:
```bash
../cardano-node/cardano-cli  query protocol-parameters \
--mainnet \
--out-file protocol.json
```
7. Create Policy V/S Key:
```bash
../cardano-node/cardano-cli address key-gen \
--verification-key-file policy.vkey \
--signing-key-file policy.skey
```
8. Get Policy Hash:
```bash
../cardano-node/cardano-cli address key-hash --payment-verification-key-file ./wallet/policy.vkey
```
9. Create Policy.script file:
nano policy.script (or your favorite text editor) with the following content:

>{ 
>"keyHash": "policy_key_hash_goes_here", 
>"type": "sig" 
>}

Save the file ;)

10. Get Policy (transaction) ID:
```bash
../cardano-node/cardano-cli transaction policyid --script-file policy.script
```
11. Create Metadata file with nano or your favorite text editor:
Compose or paste metadata and save the file with the name

metadata.json

12. Create raw transaction:
```bash
../cardano-node/cardano-cli transaction build-raw \
 --mary-era \
 --fee 300000 \
 --tx-in TXHASHHERE#0 \
 --tx-out YOURDAEDALUSADDRESSHERE+4700000+"10 YOURPOLICYIDHERE.ASSETNAME01+10 YOURPOLICYIDHERE.ASSETNAME02+10 YOURPOLICYIDHERE.ASSETNAME03+10 \
 YOURPOLICYIDHERE.ASSETNAME04+10 YOURPOLICYIDHERE.ASSETNAME05+10 YOURPOLICYIDHERE.ASSETNAME06+10 YOURPOLICYIDHERE.ASSETNAME07+10 YOURPOLICYIDHERE.ASSETNAME08" \ 
--mint="10 YOURPOLICYIDHERE.ASSETNAME01+10 YOURPOLICYIDHERE.ASSETNAME02+10 YOURPOLICYIDHERE.ASSETNAME03+10 YOURPOLICYIDHERE.ASSETNAME04+10 \
YOURPOLICYIDHERE.ASSETNAME05+10 YOURPOLICYIDHERE.ASSETNAME06+10 YOURPOLICYIDHERE.ASSETNAME07+10 YOURPOLICYIDHERE.ASSETNAME08" \
 --metadata-json-file metadata.json \
 --json-metadata-no-schema \
 --out-file matx.raw
```
13. Sign Transaction:
```bash
../cardano-node/cardano-cli transaction sign \
 --signing-key-file ./wallet/payment.skey \
 --signing-key-file ./wallet/policy.skey \
 --script-file policy.script \
 --mainnet \
 --tx-body-file matx.raw \
 --out-file matx.signed
 ```
 14. Submit Transaction:
```bash 
../cardano-node/cardano-cli transaction submit --tx-file  matx.signed --mainnet
```
### Important Note!
When you submitting the raw transaction you can directly give the adress to send minted tockens and the ADA Dust. It can be Daedalus or Yoroi Wallet Address. 

You can submit raw transaction to send minted tockens to several addresses. Than you need initially send to cli wallet 5ADA x number of address you want to send minted tokens.

### Example:
```bash
../cardano-node/cardano-cli transaction build-raw \
 --mary-era \
 --fee 300000 \
 --tx-in TXHASHHERE#0 \
 --tx-out YOURDAEDALUSADDRESSHERE+4700000+"5 YOURPOLICYIDHERE.ASSETNAME01+5 YOURPOLICYIDHERE.ASSETNAME02+5 YOURPOLICYIDHERE.ASSETNAME03+5 \
 YOURPOLICYIDHERE.ASSETNAME04+5 YOURPOLICYIDHERE.ASSETNAME05+5 YOURPOLICYIDHERE.ASSETNAME06+5 YOURPOLICYIDHERE.ASSETNAME07+5 YOURPOLICYIDHERE.ASSETNAME08" \ 
--tx-out YOURDAEDALUSADDRESS!2!HERE+5000000+"5 YOURPOLICYIDHERE.ASSETNAME01+5 YOURPOLICYIDHERE.ASSETNAME02+5 YOURPOLICYIDHERE.ASSETNAME03+5 \
 YOURPOLICYIDHERE.ASSETNAME04+5 YOURPOLICYIDHERE.ASSETNAME05+5 YOURPOLICYIDHERE.ASSETNAME06+5 YOURPOLICYIDHERE.ASSETNAME07+5 YOURPOLICYIDHERE.ASSETNAME08" \ 
--mint="10 YOURPOLICYIDHERE.ASSETNAME01+10 YOURPOLICYIDHERE.ASSETNAME02+10 YOURPOLICYIDHERE.ASSETNAME03+10 YOURPOLICYIDHERE.ASSETNAME04+10 \
YOURPOLICYIDHERE.ASSETNAME05+10 YOURPOLICYIDHERE.ASSETNAME06+10 YOURPOLICYIDHERE.ASSETNAME07+10 YOURPOLICYIDHERE.ASSETNAME08" \
 --metadata-json-file metadata.json \
 --json-metadata-no-schema \
 --out-file matx.raw
 ```
In the example above we mint 10 of each tokens and send to two addresses by 5 each. Plase make sure all ADA from initial transaction will be spent. Here we suppose 10 ADA was sent, it's 10 000 000 lovelaces 300000 fee, 4700000 to first address, 5000000 to second address = 10ADA total.
Same for minted tockens - if we mint 10 we should send 5 to first address and 5 to second, or 9 and 1 etc.

### Important note 2!
If you need to mint again, and you ok to mint with the same Policy ID, than do item 4 directly - to see wallet address (it will be exactly the same, nothing changed) Send ADA again there.
Than 5 .
Than go to item 11 to update/create new metadata,

Than just 11, 12, 13.

If you need to generate different Policy ID, than 2, 5, 8, 9, 10, 11, 12, 13.

There is no any need to regenerate any of the keys and waller address.

### Burning with Cardano CLI
1. Create RAW transcation
```bash
../cardano-node/cardano-cli transaction build-raw \
--mary-era \
--fee 300000 \
--tx-in TXHASH#0 \
--tx-out ADDRESS+4400000 \
--mint="-1 POLICYID.ASSETNAME" \
--out-file burn.raw
```
2. Sign RAW transaction
```bash
../cardano-node/cardano-cli transaction sign \
 --signing-key-file ./wallet/payment.skey \
 --signing-key-file ./wallet/policy.skey \
 --script-file policy.script \
 --mainnet \
 --tx-body-file burn.raw \
 --out-file burn.signed
```
3. Submit RAW transcation
```bash
../cardano-node/cardano-cli transaction submit --tx-file  burn.signed --mainnet
