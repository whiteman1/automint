import os
import json
import subprocess
import sys
CARDANO_CLI = "E:\\Daedalus Mainnet\\cardano-cli.exe"
PYTHON3EXE = "C:\\Python39\\python.exe" 

PIPE_NAME=subprocess.check_output(["powershell","(Get-ChildItem \\\.\\\pipe\\\ | Where-Object {$_.name -like 'cardano-node-mainnet*'}).FullName"])
PIPE_NAME = str(PIPE_NAME).split('.')
try:
    PIPE_NAME = "\\\.\\pipe\\cardano-node-mainnet."+PIPE_NAME[2]+"."+PIPE_NAME[3][0]
except:
    print("can't find pipe. Try start daedalus.")
    sys.exit(1)

print("PIPE_NAME:",PIPE_NAME)
os.environ['CARDANO_NODE_SOCKET_PATH'] = PIPE_NAME

subprocess.call([CARDANO_CLI,"get-tip" ,"--mainnet"])

param1 = "ftwkun"
param2 = "adressTosendTokens"
payment_addr=''
TX_HASH =''
KEY_HASH=''
ASSET_NAME =''

def query_utxo():
    print("query_utxo")
    f = open("./wallet_"+param1+"/payment.addr", 'r')
    payment_addr = f.read()
    print ("payment_addr:",payment_addr)
    f.close()
    TX_HASH_OUTPUT = subprocess.check_output([CARDANO_CLI,"query", "utxo", "--address", payment_addr, "--mainnet"])
    if len(str(TX_HASH_OUTPUT).split("\\r\\n")) == 3:
        print("There is no TX_IN in the created wallet yet")
        print("Need to send 5 ADA to address:")
        print(payment_addr)
    else:
        TX_HASH = str(TX_HASH_OUTPUT).split("\\r\\n")[3] 
        print("TX_HASH:",TX_HASH)

def  mint():
    print("mint")
    KEY_HASH_OUTPUT =subprocess.check_output([CARDANO_CLI,"address", "key-hash", "--payment-verification-key-file", "./wallet_"+param1+"/policy.vkey"])
    #print(KEY_HASH_OUTPUT)
    KEY_HASH = str(KEY_HASH_OUTPUT).replace('\\n\'','').replace('b\'','')
    print('KEY_HASH:',KEY_HASH)    	
    print("Generating Policy script within ./wallet_"+param1+"/")
    policy={}
    policy["keyHash"] = KEY_HASH
    policy["type"] = "sig"
    with open("./wallet_"+param1+"/policy_"+param1+".script",'w') as f:
        json.dump(policy, f)
    with open("./wallet_"+param1+"/policy_"+param1+".script",'r') as f:
        policy_script = f.read()
        print ("policy_script:", policy_script)

    ASSET_NAME_OUTPUT = subprocess.check_output([CARDANO_CLI, "transaction", "policyid", "--script-file", "./wallet_"+param1+"/policy_"+param1+".script"])
    ASSET_NAME = str(ASSET_NAME_OUTPUT).replace('\\r\\n\'','').replace('b\'','')
    with open("./wallet_"+param1+"/"+param1+"_metadata_template.json",'w') as f:
        metadata={}        
        metadata["721"]={}
        metadata["721"][ASSET_NAME]={}
        metadata["721"][ASSET_NAME]["NFTnameHere1"]={}
        metadata["721"][ASSET_NAME]["NFTnameHere1"]["id"]=1
        metadata["721"][ASSET_NAME]["NFTnameHere1"]["image"]="ipfs://IPFS_CID_HERE"
        metadata["721"][ASSET_NAME]["NFTnameHere1"]["name"]="NFTnameHere1"
        metadata["721"][ASSET_NAME]["NFTnameHere1"]["quantity"]=1
        metadata["721"][ASSET_NAME]["NFTnameHere1"]["copyright"]="© 2021 ftwkun"
#        metadata["721"][ASSET_NAME]["NFTnameHere1"]["site"]=""
        metadata["721"][ASSET_NAME]["NFTnameHere1"]["twitter"]="https://twitter.com/ftwkun"
#        metadata["721"][ASSET_NAME]["NFTnameHere1"]["etc"]="somethingelse"
        json.dump(metadata, f, sort_keys = True,indent=4)
    print("Metadata JSON Template was genereted: ./wallet_"+param1+"/"+param1+"_metadata_template.json")
    print("Edit ./wallet_" + param1 + "/" + "_metadata_template.json and save it as ./wallet_" + param1 + "/" + param1 + "_metadata.json:")  
    f = open("./wallet_" + param1 + "/" + param1 + "_metadata_template.json", 'r')
    metadata_template = f.read()
    print ("metadata_template:", metadata_template)
    f.close()

    print("Provide a path to your edited Metadata JSON File or press Enter to load $meta_json_path")
    meta_json_path = input()
    if meta_json_path=="":
        meta_json_path="./wallet_" + param1 + "/" + param1 + "_metadata.json"
        print("loading from default path for edited file: $meta_json_path")
    print("Metadata JSON path: " + meta_json_path)
    JSON_PARSED_NAME_LINE = subprocess.check_output([PYTHON3EXE, "parse_metadata_json.py",meta_json_path])
    JSON_PARSED_NAMES = str(JSON_PARSED_NAME_LINE).replace("b'",'').split("\\r\\n")[0]
    #print("names:",JSON_PARSED_NAMES)
    JSON_PARSED_QUANTITY_LINE = str(JSON_PARSED_NAME_LINE).replace("'",'').split("\\r\\n")[1]
    #print("quantities:",JSON_PARSED_QUANTITY_LINE)
    
    
    print("Going to mint:")
    quantitieslist = JSON_PARSED_QUANTITY_LINE.split(" ")
    #print(quantitieslist)
    i=0
    mint_line = ''
    for n in JSON_PARSED_NAMES.split(" "):
        q =quantitieslist[i] 
        if q:
            print(q," Asset(s) with name ",n)
            mint_line += q+" "+ASSET_NAME+"."+n
            i+=1
    
    print("mint_line:",mint_line)
    print("If the above correct, hit any button to mint")
    input()

    print("mint TO:",param2)
    print("TX_HASH:",TX_HASH)

    subprocess.call([CARDANO_CLI, "transaction", "build-raw",
                     "--mary-era",
                     "--fee",900000,
                     "--tx-in",TX_HASH,
		     "--tx-out",param2+14100000+"\""+mint_line+"\"",
                     "--mint=\""+mint_line+"\"",
                     "--metadata-json-file", meta_json_path,
                     "--json-metadata-no-schema",
                     "--out-file", "./wallet_"+param1+"/matx.raw"])

    subprocess.call([CARDANO_CLI, "transaction", "sign",
                     "--signing-key-file", "./wallet_"+param1+"/payment.skey",
                     "--signing-key-file", "./wallet_"+param1+"/policy.skey",
                     "--script-file ./wallet_"+param1+"/policy_"+param1+".script",
                     "--mainnet",
                     "--tx-body-file","./wallet_"+param1+"/matx.raw",
                     "--out-file", "./wallet_"+param1+"/matx.signed"])

    subprocess.call([CARDANO_CLI, "transaction", "submit",
                     "--tx-file", "./wallet_"+param1+"/matx.signed",
                     "--mainnet"])
'''
		./cardano-node-1.26.1-macos/cardano-cli transaction build-raw \
		--mary-era \
		--fee 900000 \
		--tx-in $TX_HASH#0 \
		--tx-out $2+14100000+"$mint_line" \
		--mint="$mint_line" \
		--metadata-json-file $meta_json_path \
		--json-metadata-no-schema \
		--out-file ./wallet_$1/matx.raw

		./cardano-node-1.26.1-macos/cardano-cli transaction sign \
		--signing-key-file ./wallet_$1/payment.skey \
		--signing-key-file ./wallet_$1/policy.skey \
		--script-file ./wallet_$1/policy_$1.script \
		--mainnet \
		--tx-body-file ./wallet_$1/matx.raw \
		--out-file ./wallet_$1/matx.signed

		./cardano-node-1.26.1-macos/cardano-cli transaction submit --tx-file  ./wallet_$1/matx.signed --mainnet
	}
'''
def checkPaymentAddr():
    if os.path.isfile("./wallet_"+param1+"/payment.addr"):
        print("Wallet ./wallet_"+param1+" exists. You can not overwrite existing wallet")
        print("Proceeding to Query UTXO")
        query_utxo()
        mint()
    else:
        print("Wallet ./wallet_"+param1+" was not previosly used. Crating Wallet ./wallet_"+param1)
        os.mkdir("./wallet_"+param1)
        gen_address_policy()
        print("Send 5ADA to the following address:")
        f = open("./wallet_"+param1+"/payment.addr", 'r')
        file_contents = f.read()
        print (file_contents)
        f.close()
        input()
        query_utxo()
        mint()

def gen_address_policy():
    subprocess.call([CARDANO_CLI, "address", "key-gen" ,
                     "--verification-key-file", "./wallet_" + param1 + "/payment.vkey",
		     "--signing-key-file", "./wallet_" + param1 + "/payment.skey"])

    subprocess.call([CARDANO_CLI, "address", "build" ,
                     "--payment-verification-key-file", "./wallet_" + param1 + "/payment.vkey",
		     "--out-file", "./wallet_" + param1 + "/payment.addr", "--mainnet"])

    subprocess.call([CARDANO_CLI, "query","protocol-parameters" , "--mainnet",
		     "--out-file", "protocol.json"])

    subprocess.call([CARDANO_CLI, "address", "key-gen",
                     "--verification-key-file", "./wallet_" + param1 + "/policy.vkey",
		     "--signing-key-file", "./wallet_" + param1 + "/policy.skey"])

if __name__ == "__main__":
    checkPaymentAddr()